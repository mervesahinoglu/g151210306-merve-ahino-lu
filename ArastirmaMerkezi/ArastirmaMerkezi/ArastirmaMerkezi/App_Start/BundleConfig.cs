﻿using System.Web;
using System.Web.Optimization;

namespace ArastirmaMerkezi
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/jquery.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/smoothscroll.js",
                      "~/Scripts/isotope.js",
                      "~/Scripts/imagesloaded.min.js",
                      "~/Scripts/nivo-lightbox.min.js",
                      "~/Scripts/jquery.backstretch.min.js",
                      "~/Scripts/wow.min.js",
                      "~/Scripts/custom.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/nivo_themes/default/default.css",
                      "~/Content/animate.min.css",
                      "~/Content/bootstrap.min.css",
                      "~/Content/et-line-font.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/nivo-lightbox.css",
                      "~/Content/style.css"));
        }
    }
}
